﻿using task3.Interfaces;
using task3.Models.Shapes;
using task3.Models;

IRenderer vectorRenderer = new VectorRenderer();
IRenderer rasterRenderer = new RasterRenderer();

Shape circle = new Circle(vectorRenderer);
Shape square = new Square(rasterRenderer);
Shape triangle = new Triangle(vectorRenderer);

circle.Draw();
square.Draw();
triangle.Draw();