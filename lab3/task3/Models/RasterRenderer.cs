﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task3.Interfaces;

namespace task3.Models
{
	public class RasterRenderer : IRenderer
	{
		public void RenderShape(string shape)
		{
			Console.WriteLine($"Drawing {shape} as pixels");
		}
	}
}
