﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task3.Interfaces;

namespace task3.Models.Shapes
{
	public class Circle(IRenderer renderer) : Shape(renderer)
	{
		public override void Draw()
		{
			renderer.RenderShape("Circle");
		}
	}
}
