﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task3.Interfaces;

namespace task3.Models
{
	public abstract class Shape(IRenderer renderer)
	{
		protected IRenderer renderer = renderer;

		public abstract void Draw();
	}
}
