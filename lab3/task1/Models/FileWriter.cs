﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task1.Interfaces;

namespace task1.Models
{
	public class FileWriter(string filePath) : IDisposable
	{
		private StreamWriter? _streamWriter = new(filePath, true);

		public void Write(string text)
		{
			_streamWriter?.Write(text);
		}

		public void WriteLine(string text)
		{
			_streamWriter?.WriteLine(text);
		}

		public void Dispose()
		{
			if (_streamWriter != null)
			{
				_streamWriter.Dispose();
				_streamWriter = null;
			}
		}
	}

}
