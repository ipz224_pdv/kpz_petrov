﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task4.Interfaces;

namespace task4.Models
{
	public class SmartTextReader : ITextReader
	{
		public List<List<char>> ReadText(string filename)
		{
			List<List<char>> text = [];
			try
			{
				using (StreamReader sr = new(filename))
				{
					string line;
					while ((line = sr.ReadLine()) != null)
					{
						text.Add(new List<char>(line.ToCharArray()));
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("An error occurred: " + ex.Message);
			}
			return text;
		}
	}
}
