﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task4.Interfaces;

namespace task4.Models
{
	public class SmartTextChecker(ITextReader realReader) : ITextReader
	{
		private readonly ITextReader realReader = realReader;

		public List<List<char>> ReadText(string filename)
		{
			Console.WriteLine("Opening file: " + filename);
			var text = realReader.ReadText(filename);
			Console.WriteLine("File read successfully");
			Console.WriteLine("Number of lines: " + text.Count);
			int totalChars = 0;
			foreach (var line in text)
			{
				totalChars += line.Count;
			}
			Console.WriteLine("Number of characters: " + totalChars);
			Console.WriteLine("Closing file");
			return text;
		}
	}
}
