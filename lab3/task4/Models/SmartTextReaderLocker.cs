﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using task4.Interfaces;

namespace task4.Models
{
	public class SmartTextReaderLocker(ITextReader realReader, string regexPattern) : ITextReader
	{
		private readonly ITextReader realReader = realReader;
		private readonly Regex pattern = new(regexPattern);

		public List<List<char>> ReadText(string filename)
		{
			if (pattern.IsMatch(filename))
			{
				Console.WriteLine("Access denied!");
				return [];
			}
			else
			{
				return realReader.ReadText(filename);
			}
		}
	}
}
