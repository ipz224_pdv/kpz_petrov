﻿using task4.Interfaces;
using task4.Models;

ITextReader realReader = new SmartTextReader();
ITextReader checkerProxy = new SmartTextChecker(realReader);
ITextReader lockerProxy = new SmartTextReaderLocker(realReader, "limited_.*");

Console.WriteLine("Reading regular file:");
checkerProxy.ReadText("regular_file.txt");
Console.WriteLine();

Console.WriteLine("Reading limited file:");
lockerProxy.ReadText("limited_file.txt");