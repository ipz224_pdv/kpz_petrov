﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task2.Interfaces;

namespace task2.Models.Inventory
{
	public class Amulet : IInventory
	{
		public void Equip()
		{
			Console.WriteLine("Equipping Amulet");
		}
	}
}
