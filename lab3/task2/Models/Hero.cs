﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task2.Interfaces;

namespace task2.Models
{
	public abstract class Hero
	{
		public abstract void Display();

		protected IInventory? inventory;

		public void SetInventory(IInventory inventory)
		{
			this.inventory = inventory;
		}

		public void EquipInventory()
		{
			inventory?.Equip();
		}
	}

}
