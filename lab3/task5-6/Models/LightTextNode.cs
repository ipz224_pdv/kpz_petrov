﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5.Models
{
	public class LightTextNode(string text) : LightNode
	{
		private readonly string text = text;

		public override string OuterHTML()
		{
			return text;
		}

		public override string InnerHTML()
		{
			return text;
		}
	}
}
