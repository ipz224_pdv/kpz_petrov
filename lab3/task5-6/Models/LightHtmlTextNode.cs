﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5.Models
{
	public class LightHtmlTextNode(string text) : LightHtmlElement
	{
		private readonly string text = text;

		public override void Render()
		{
			Console.WriteLine(text);
		}
	}
}
