﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5.Models
{
	public class LightElementNode : LightNode
	{
		private readonly string tagName;
		private readonly string displayType;
		private readonly string closingType;
		private readonly List<LightNode> children;
		private readonly List<string> cssClasses;

		public LightElementNode(string tagName, string displayType, string closingType, List<string> cssClasses)
		{
			this.tagName = tagName;
			this.displayType = displayType;
			this.closingType = closingType;
			this.cssClasses = cssClasses;
			this.children = [];
		}

		public void AddChild(LightNode child)
		{
			children.Add(child);
		}

		public override string OuterHTML()
		{
			StringBuilder sb = new();
			sb.Append($"<{tagName} ");
			if (cssClasses.Count > 0)
			{
				sb.Append("class=\"");
				sb.Append(string.Join(" ", cssClasses));
				sb.Append("\" ");
			}
			sb.Append(">");
			sb.Append(InnerHTML());
			sb.Append($"</{tagName}>");
			return sb.ToString();
		}

		public override string InnerHTML()
		{
			StringBuilder sb = new();
			foreach (var child in children)
			{
				sb.Append(child.OuterHTML());
			}
			return sb.ToString();
		}
	}
}
