﻿using System;
using System.IO;
using task5.Models;


// Task 5
LightElementNode body = new("body", "block", "closing", []);
LightElementNode div = new("div", "block", "closing", ["container"]);
LightElementNode h1 = new("h1", "block", "closing", []);
LightTextNode text = new("Hello, LightHTML!");

body.AddChild(div);
div.AddChild(h1);
h1.AddChild(text);

Console.WriteLine(body.OuterHTML());



// Task 6

string fileName = "pg1513.txt";

string rootPath = Directory.GetCurrentDirectory();
string filePath = Path.Combine(rootPath, fileName);
string[] lines = File.ReadAllLines(filePath);

LightHtmlFactory factory = new();

bool firstLine = true;
foreach (var line in lines)
{
	LightHtmlElement element = factory.GetElement(line);
	if (element == null)
	{
		continue;
	}
	if (firstLine)
	{
		element = new LightHtmlElementNode("h1");
		firstLine = false;
	}
	if (element is LightHtmlTextNode)
	{
		element.Render();
	}
	else
	{
		LightHtmlElement textNode = factory.GetTextNode(line);
		((LightHtmlElementNode)element).AddChild(textNode);
		element.Render();
	}

	
}