﻿using ClassLibrary.Models;

class Program
{
	static void Main(string[] args)
	{
		Currency usd = new Currency("US Dollar", "USD", 1.0);

		Money price = new Money(500, 0, usd);

		Category electronicsCategory = new Category("Electronics", "Electronic devices");
		Category mobileCategory = new Category("Mobile", "Mobile phones");

		List<Category> phoneCategories = new List<Category> { electronicsCategory, mobileCategory };
		Product phone = new Product("Phone", price, phoneCategories);

		Warehouse warehouse = new Warehouse();

		warehouse.AddProduct(phone);

		Reporting reporting = new Reporting(warehouse);
		reporting.RegisterIncoming(phone, 10);

		reporting.RegisterOutgoing(phone, 3);

		reporting.GenerateInventoryReport();

		Cart cart = new Cart();

		cart.AddToCart(phone);
		cart.AddToCart(phone);

		// cart.RemoveFromCart(phone);

		cart.DisplayCart();
	}
}