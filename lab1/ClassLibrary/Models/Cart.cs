﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Models
{
	public class Cart
	{
		private List<Product> products = new List<Product>();

		// Метод для додавання товару до корзини
		public void AddToCart(Product product)
		{
			products.Add(product);
			Console.WriteLine($"Product '{product.Name}' added to cart.");
		}

		public void RemoveFromCart(Product product)
		{
			if (products.Remove(product))
			{
				Console.WriteLine($"Product '{product.Name}' removed from cart.");
			}
			else
			{
				Console.WriteLine($"Product '{product.Name}' not found in cart.");
			}
		}

		public Money CalculateTotalPrice()
		{
			double totalPrice = 0;
			foreach (var product in products)
			{
				totalPrice += product.Price.WholePart + (double)product.Price.FractionalPart / 100;
			}
			return new Money((int)totalPrice, (int)((totalPrice - (int)totalPrice) * 100), products[0].Price.Currency);
		}

		public void DisplayCart()
		{
			Console.WriteLine("Products in cart:");
			foreach (var product in products)
			{
				product.Price.DisplayPrice();
			}
			Console.WriteLine($"Total price:");
			CalculateTotalPrice().DisplayPrice();
		}
	}
}
