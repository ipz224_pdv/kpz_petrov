﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Models
{
	public class Money
	{
		public int WholePart { get; set; }
		public int FractionalPart { get; set; }
		public Currency Currency { get; set; }

		public Money(int wholePart, int fractionalPart, Currency currency)
		{
			WholePart = wholePart;
			FractionalPart = fractionalPart;
			Currency = currency;
		}

		public void DisplayPrice()
		{
			Console.WriteLine($"{WholePart}.{FractionalPart} {Currency.Symbol}");
		}
		public void AddMoney(int wholePart, int fractionalPart)
		{
			WholePart += wholePart;
			FractionalPart += fractionalPart;

			if (FractionalPart >= 100)
			{
				WholePart += FractionalPart / 100;
				FractionalPart %= 100;
			}
		}
		public void SubtractMoney(int wholePart, int fractionalPart)
		{
			int totalFractionalPart = WholePart * 100 + FractionalPart;
			int amountToSubtract = wholePart * 100 + fractionalPart;

			if (totalFractionalPart >= amountToSubtract)
			{
				totalFractionalPart -= amountToSubtract;
				WholePart = totalFractionalPart / 100;
				FractionalPart = totalFractionalPart % 100;
			}
			else
			{
				Console.WriteLine("Insufficient funds to subtract.");
			}
		}
		public bool IsEqual(Money otherMoney)
		{
			return WholePart == otherMoney.WholePart && FractionalPart == otherMoney.FractionalPart && Currency == otherMoney.Currency;
		}
	}

}
