﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Models
{
	public class Warehouse
	{
		private List<Product> products = new List<Product>();
		public DateTime LastAdditionDate { get; private set; }

		public void AddProduct(Product product)
		{
			products.Add(product);
			LastAdditionDate = DateTime.Now;
		}

		public void RemoveProduct(Product product)
		{
			products.Remove(product);
		}

		public void DisplayAllProducts()
		{
			Console.WriteLine("Products in warehouse:");
			foreach (var product in products)
			{
				product.DisplayProductInfo();
			}
		}
	}
}
