﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2.Interfaces
{
	public interface IDevice
	{
		string? Brand { get; set; }
		string? Model { get; set; }

		void DisplayInfo();
	}

}
