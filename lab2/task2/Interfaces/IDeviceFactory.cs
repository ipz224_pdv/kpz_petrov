﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2.Interfaces
{
	public interface IDeviceFactory
	{
		IDevice CreateDevice(string brand, string model);
	}

}
