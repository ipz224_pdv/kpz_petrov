﻿using task5.Interfaces;

namespace task5.Models
{
	public class HeroDirector(IHeroBuilder builder)
	{
		private readonly IHeroBuilder heroBuilder = builder;

		public void ConstructHero(string name, string height, string build, string hairColor, string eyeColor, string clothing, List<string> inventory)
		{
			heroBuilder
				.SetName(name)
				.SetHeight(height)
				.SetBuild(build)
				.SetHairColor(hairColor)
				.SetEyeColor(eyeColor)
				.SetClothing(clothing);

			foreach (var item in inventory)
			{
				heroBuilder.AddToInventory(item);
			}
		}

		public void ConstructVillain(string name, string height, string build, string hairColor, string eyeColor, string clothing, List<string> inventory)
		{
			heroBuilder
				.SetName(name)
				.SetHeight(height)
				.SetBuild(build)
				.SetHairColor(hairColor)
				.SetEyeColor(eyeColor)
				.SetClothing(clothing)
				.SetType("Villain");

			foreach (var item in inventory)
			{
				heroBuilder.AddToInventory(item);
			}
		}
	}
}
