﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2.Models
{
	public class Runway
	{
		public readonly Guid Id = Guid.NewGuid();
		public Aircraft? OccupyingAircraft { get; private set; }

		public bool IsAvailable()
		{
			return OccupyingAircraft == null;
		}

		public bool IsBusyWith(Aircraft aircraft)
		{
			return OccupyingAircraft == aircraft;
		}

		public void LandAircraft(Aircraft aircraft)
		{
			OccupyingAircraft = aircraft;
			Console.WriteLine($"Aircraft {aircraft.Name} Приземлився на злітно-посадкову смугу {Id}.");
		}

		public void TakeOffAircraft(Aircraft aircraft)
		{
			OccupyingAircraft = null;
			Console.WriteLine($"Aircraft {aircraft.Name} Відлетів з злітно-посадкової смуги {Id}.");
		}
	}
}
