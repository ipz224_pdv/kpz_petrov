﻿using task2.Models;

var rw1 = new Runway();
var rw2 = new Runway();
var JetX = new Aircraft("JetX");
var CargoMax = new Aircraft("CargoMax");

var commandCentre = new CommandCentre([rw1, rw2], [JetX, CargoMax]);

JetX.Land(rw1); // Successful landing
CargoMax.Land(rw2); // Successful landing
JetX.TakeOff(rw2); // Error: Not on runway 2
CargoMax.TakeOff(rw1);