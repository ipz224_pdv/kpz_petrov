﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5.Models
{
    // Клас TextEditor, що реалізує функціонал збереження та скасування змін у текстовому документі
    public class TextEditor
    {
        private readonly TextDocument document;
        private readonly Stack<Memento> history;

        public TextEditor(TextDocument document)
        {
            this.document = document;
            history = new Stack<Memento>();

            SaveState();
        }

        public string Text
        {
            get => document.Text;
            set => document.Text = value;
        }

        public void Write(string text)
        {
            SaveState();
            document.Text += text;
        }

        public void Undo()
        {
            if (history.Count > 1) // Перевірка, чи є що скасовувати
            {
                history.Pop(); // Видаляємо поточний стан
                document.Text = history.Peek().TextSnapshot; // Відновлюємо попередній стан
            }
        }

        private void SaveState()
        {
            history.Push(new Memento(document.Text)); // Зберігаємо стан тексту
        }
    }
}
