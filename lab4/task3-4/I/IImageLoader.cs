﻿using System;
using task3.Models.Image;

namespace task3.I
{
    public interface IImageLoader
	{
		void LoadImage(LightHtmlImageNode image);
	}
}
