﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task3.Models;
using task5.Models;

namespace task3.Models.Element
{
    public class LightElementNode : LightNode
    {
        private readonly string tagName;
        private readonly string displayType;
        private readonly string closingType;
        private readonly List<LightNode> children;
        private readonly List<string> cssClasses;
        private List<EventListener> eventListeners = [];
        public LightElementNode(string tagName, string displayType, string closingType, List<string> cssClasses)
        {
            this.tagName = tagName;
            this.displayType = displayType;
            this.closingType = closingType;
            this.cssClasses = cssClasses;
            children = [];
        }

        public void AddChild(LightNode child)
        {
            children.Add(child);
        }

        public override string OuterHTML()
        {
            StringBuilder sb = new();
            sb.Append($"<{tagName} ");
            if (cssClasses.Count > 0)
            {
                sb.Append("class=\"");
                sb.Append(string.Join(" ", cssClasses));
                sb.Append("\" ");
            }
            sb.Append(">");
            sb.Append(InnerHTML());
            sb.Append($"</{tagName}>");
            return sb.ToString();
        }

        public override string InnerHTML()
        {
            StringBuilder sb = new();
            foreach (var child in children)
            {
                sb.Append(child.OuterHTML());
            }
            return sb.ToString();
        }
        public void Click()
        {
            if (eventListeners != null)
            {
                var listeners = eventListeners.Where(listener => listener.EventType == "click");
                foreach (var listener in listeners)
                {
                    listener.EventHandler();
                }
            }
        }

        public void MouseOver()
        {
            if (eventListeners != null)
            {
                var listeners = eventListeners.Where(listener => listener.EventType == "mouseover");
                foreach (var listener in listeners)
                {
                    listener.EventHandler();
                }
            }
        }


        public void AddEventListener(string eventType, Action eventHandler)
        {
            if (eventListeners == null)
            {
                eventListeners = new List<EventListener>();
            }

            eventListeners.Add(new EventListener(eventType, eventHandler));
        }

        public void RemoveEventListener(string eventType)
        {
            if (eventListeners != null)
            {
                eventListeners.RemoveAll(listener => listener.EventType == eventType);
            }
        }

    }
}
