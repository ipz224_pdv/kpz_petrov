﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using task3.I;

namespace task3.Models.Image
{
    public class NetworkImageLoader : IImageLoader
	{
		public void LoadImage(LightHtmlImageNode image)
		{
			using (var webClient = new WebClient())
			{
				image.Content = webClient.DownloadData(image.Src);
			}
		}
	}
}
