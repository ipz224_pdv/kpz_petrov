﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task3.I;

namespace task3.Models.Image
{
    public class LightHtmlImageNode : LightHtmlElement
    {
        private readonly IImageLoader imageLoader;
        public string? Src { get; set; }
        public byte[] Content { get; internal set; }

        public LightHtmlImageNode(string src)
        {
            Console.OutputEncoding = Encoding.UTF8;

            Src = src;
            if (src.StartsWith("http") || src.StartsWith("https"))
            {
                imageLoader = new NetworkImageLoader();
                Console.WriteLine("Зображення із інтернету");
            }
            else
            {
                imageLoader = new FileSystemImageLoader();
                Console.WriteLine("Зображення із файлової системи");
            }

        }

        public override void Render()
        {
            imageLoader.LoadImage(this);
			Console.Write($"<img src= '{Src}'>");
			Console.WriteLine($"</img>");
		}
    }
}
